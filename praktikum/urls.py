
"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import lab_8.urls as lab_8
import lab_9.urls as lab_9
import lab_10.urls as lab_10

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^lab-8/', include(lab_8, namespace='lab-8')),
	url(r'^lab-9/', include(lab_9, namespace='lab-9')),
	url(r'^lab-10/', include(lab_10, namespace='lab-10')),
	url(r'^$',RedirectView.as_view(url="/lab-8/",permanent = "true"), name='index'),
]
